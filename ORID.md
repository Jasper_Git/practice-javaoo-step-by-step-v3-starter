## Objective

- In the morning, we first learned Code Review and its rules, and then we carried out Code Review of yesterday's homework POS-Machine. After that, I learned some common Stream APIs in Java and did the corresponding exercises.
- In the afternoon, it was mainly introduced to the face of object programming, such as what is the object -oriented and how to understand its packaging, inheritance, and polymorphism. Subsequently, some exercises and showcase were performed based on these three characteristics.

## Reflective

- At when I Code Review of yesterday's homework, I first learned the coding advantages of others in it. Secondly, the team members can help the other party find an unlimited coding error or make optimization suggestions. This method can make everyone happy together later Earth progress.
- At afternoon -oriented courses, I have a deeper understanding of the objects in the program.

## Interpretive

- The understand of object -oriented: The object -oriented is to abstract things into one -one -one object. The object has characteristics or state, and the object has some behaviors that can change its attributes.
- In my understanding, there are not only Stream APIs in functional programming, but also functional interfaces, Lambada Expressions, method references, optional, etc. Sample operation.

## Decision

- In the future, when I encounter the operation of the data flow, I can use the Stream API as much as possible.