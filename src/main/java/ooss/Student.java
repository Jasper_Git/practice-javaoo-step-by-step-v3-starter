package ooss;

public class Student extends Person {

    private Klass klass;
    private boolean isLeader;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (klass != null && this.klass != null) {
            return klass.getNumber() == this.klass.getNumber();
        }
        return false;
    }

    public Klass getKlass() {
        return klass;
    }

    public void setLeader(boolean leader) {
        isLeader = leader;
    }

    @Override
    public String introduce() {
        if (klass == null) {
            return super.introduce() + " I am a student.";
        } else if (!this.isLeader) {
            return super.introduce() + String.format(" I am a student. I am in class %d.", this.klass.getNumber());
        } else {
            return super.introduce() + String.format(" I am a student. I am the leader of class %d.", this.klass.getNumber());
        }
    }

    // student 订阅 leader
    @Override
    public void LeaderHasUpdated(Student leader) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n",
                this.getName(), leader.getKlass().getNumber(), leader.getName());
    }
}
