package ooss;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> klasses = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    public List<Klass> getKlasses() {
        return klasses;
    }

    public void assignTo(Klass klass) {
        if (!klasses.contains(klass)) {
            klasses.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return klasses.contains(klass);
    }

    @Override
    public String introduce() {

        List<String> collect = klasses.stream().map(Klass::getNumber).map(String::valueOf).collect(Collectors.toList());
        String[] res = new String[klasses.size()];
        collect.toArray(res);
        if (klasses.size() != 0) {
            return super.introduce() + String.format(" I am a teacher. I teach Class %s.", String.join(", ", res));
        }
        return super.introduce() + " I am a teacher.";
    }

    public boolean isTeaching(Student student) {
        if (student.getKlass() != null) {
            if (this.klasses.contains(student.getKlass())) {
                return true;
            }
        }
        return false;
    }

    // teacher订阅 leader
    @Override
    public void LeaderHasUpdated(Student leader) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n",
                this.getName(), leader.getKlass().getNumber(), leader.getName());
    }
}
