package ooss;

import java.util.Objects;

public class Person {

    protected Integer id;
    protected String name;
    protected Integer age;

    public Person(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String introduce() {
        return String.format("My name is %s. I am %d years old.", this.name, this.age);
    }

    @Override
    public boolean equals(Object o) {
        Person person = (Person) o;
        return this.id == person.id;
    }

    public void LeaderHasUpdated(Student leader) {
    }
}
