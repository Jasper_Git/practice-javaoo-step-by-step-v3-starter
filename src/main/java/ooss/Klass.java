package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private int number;
    private Student leader;

    private List<Teacher> attachedTeachers = new ArrayList<>();
    private List<Student> attachedStudents = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        Klass other = (Klass) obj;
        return this.number == other.number;
    }

    public boolean isLeader(Student student) {
        return student.equals(this.leader);
    }

    public void assignLeader(Student student) {
        if (student.getKlass() == null || student.getKlass().getNumber() != this.number) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = student;
        student.setLeader(true);
        notifyAttachedPersons(student);
    }


    // assignLeader 时，通知订阅者
    public void notifyAttachedPersons(Student student) {
        this.attachedTeachers.forEach(teacher -> teacher.LeaderHasUpdated(student));
        this.attachedStudents.forEach(stu -> stu.LeaderHasUpdated(student));
    }

    // 添加订阅者
    public void attach(Person person) {
        if (person instanceof Teacher) {
            attachedTeachers.add((Teacher) person);
        }
        if (person instanceof Student) {
            attachedStudents.add((Student) person);
        }
    }

    // 移除订阅者
    public void remove(Person person) {
        if (person instanceof Teacher) {
            int index = attachedTeachers.indexOf(person);
            if (index >= 0) {
                attachedTeachers.remove(person);
            }
        }
        if (person instanceof Student) {
            int index = attachedTeachers.indexOf(person);
            if (index >= 0) {
                attachedTeachers.remove(person);
            }
        }
    }
}
